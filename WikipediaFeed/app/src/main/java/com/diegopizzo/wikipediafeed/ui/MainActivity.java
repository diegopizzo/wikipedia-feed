package com.diegopizzo.wikipediafeed.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.diegopizzo.wikipediafeed.R;
import com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment.WikiFeedFragment;

public class MainActivity extends AppCompatActivity implements WikiFeedFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (null == savedInstanceState) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, WikiFeedFragment.newInstance(savedInstanceState),
                            WikiFeedFragment.TAG_WIKI_FEED_FRAGMENT)
                    .commit();
        }
    }
}
