package com.diegopizzo.wikipediafeed.business.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diegopizzo on 25/10/2017.
 */

public class WikiModel {

    @SerializedName("tfa")
    @Expose
    private Tfa tfa;
    @SerializedName("mostread")
    @Expose
    private Mostread mostread;

    public Tfa getTfa() {
        return tfa;
    }

    public void setTfa(final Tfa tfa) {
        this.tfa = tfa;
    }

    public WikiModel withTfa(final Tfa tfa) {
        this.tfa = tfa;
        return this;
    }

    public Mostread getMostread() {
        return mostread;
    }

    public void setMostread(final Mostread mostread) {
        this.mostread = mostread;
    }

    public WikiModel withMostread(final Mostread mostread) {
        this.mostread = mostread;
        return this;
    }

}
