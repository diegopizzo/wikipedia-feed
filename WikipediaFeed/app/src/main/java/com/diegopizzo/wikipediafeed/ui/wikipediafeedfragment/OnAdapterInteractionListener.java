package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

/**
 * Created by diegopizzo on 28/10/2017.
 */

public interface OnAdapterInteractionListener {
    void enlargePhotoOfSourceArticleClicked(String path);
}
