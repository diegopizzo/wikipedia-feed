package com.diegopizzo.wikipediafeed.config.dagger;

import com.diegopizzo.wikipediafeed.business.network.WikiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by diegopizzo on 27/09/2017.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(final GsonConverterFactory gsonConverterFactory) {
        //TODO endpoint in build.gradle??
        return new Retrofit.Builder()
                .baseUrl(WikiService.SERVICE_ENDPOINT)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory(final Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().serializeNulls().create();
    }


    @Provides
    @Singleton
    WikiService provideWikiService(final Retrofit retrofit) {
        return retrofit.create(WikiService.class);
    }
}
