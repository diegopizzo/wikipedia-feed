package com.diegopizzo.wikipediafeed.business.interactor;

import com.diegopizzo.wikipediafeed.business.network.WikiService;
import com.diegopizzo.wikipediafeed.business.network.model.WikiModel;

import io.reactivex.Observable;

/**
 * Created by diegopizzo on 26/10/2017.
 */

public class WikiInteractor {

    private final WikiService wikiService;

    public WikiInteractor(final WikiService wikiService) {
        this.wikiService = wikiService;
    }

    public Observable<WikiModel> getFeedNews(final String year, final String month, final String day) {
        return wikiService.getWikiFeed(year, month, day);
    }
}
