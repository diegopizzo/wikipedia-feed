package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

import android.support.annotation.NonNull;
import android.util.Log;

import com.diegopizzo.wikipediafeed.business.interactor.WikiInteractor;
import com.diegopizzo.wikipediafeed.business.network.model.WikiModel;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by diegopizzo on 26/10/2017.
 */

public class WikiFeedFragmentPresenter implements WikiFeedFragmentContract.Presenter {

    private final WikiFeedFragmentContract.View view;
    private final WikiInteractor wikiInteractor;

    public WikiFeedFragmentPresenter(final WikiFeedFragmentContract.View view, final WikiInteractor wikiInteractor) {
        this.view = view;
        this.wikiInteractor = wikiInteractor;
    }


    @Override
    public void wikiFeedNews() {

        final String day = getStringCurrentDayOfMonth();
        final String month = getStringCurrentMonth();
        final String year = getStringCurrentYear();

        Log.i("info", day + "/" + month + "/" + year);

        final Observable<WikiModel> wikiFeedObserveble = wikiInteractor.getFeedNews(year, month, day);
        final Disposable disposable = wikiFeedObserveble.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(final Disposable disposable) throws Exception {
                        view.showLoading();
                    }
                })
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        view.showContent();
                    }
                })
                .subscribe(new Consumer<WikiModel>() {

                    @Override
                    public void accept(final WikiModel wikiModel) throws Exception {
                        view.setComponents(wikiModel);
                        Log.i("info", "Ok");
                    }
                }, new Consumer<Throwable>() {

                    @Override
                    public void accept(final Throwable throwable) throws Exception {
                        view.showMessage("Errore " + throwable.getMessage());
                        Log.e("error", throwable.getMessage());
                    }
                });
    }

    private String getStringCurrentDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    @NonNull
    private String getStringCurrentDayOfMonth() {
        return getStringCurrentDate().substring(0, 2);
    }

    @NonNull
    private String getStringCurrentMonth() {
        return getStringCurrentDate().substring(3, 5);
    }

    @NonNull
    private String getStringCurrentYear() {
        return getStringCurrentDate().substring(6);
    }
}