package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.diegopizzo.wikipediafeed.R;
import com.diegopizzo.wikipediafeed.business.network.model.Article;
import com.diegopizzo.wikipediafeed.business.network.model.Thumbnail;

import java.util.List;

/**
 * Created by diegopizzo on 27/10/2017.
 */

public class WikiMostReadAdapter extends RecyclerView.Adapter<WikiMostReadAdapter.ViewHolder> {

    // Store a member variable for the items
    private final List<Article> articles;

    // Store the context for easy access
    private final Context mContext;

    private final OnAdapterInteractionListener onAdapterInteractionListener;


    public WikiMostReadAdapter(final List<Article> articles, final Context context, final OnAdapterInteractionListener adapterListener) {
        this.articles = articles;
        mContext = context;
        onAdapterInteractionListener = adapterListener;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        final View repoView = inflater.inflate(R.layout.wiki_mostread_item, parent, false);

        // Return a new holder instance
        return new ViewHolder(repoView, onAdapterInteractionListener);
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // Get the data model based on position
        final Article article = articles.get(position);

        // Set item views based on your views and data model
        final TextView displayTitleTextView = holder.displayTitle;
        displayTitleTextView.setText(article.getNormalizedtitle());
        final ImageButton articleImageButton = holder.articleImageButton;
        if (article.getThumbnail() != null) {
            final Thumbnail thumbnail = article.getThumbnail();
            Glide.with(mContext).load(thumbnail.getSource()).into(articleImageButton);
            articleImageButton.setMinimumHeight(thumbnail.getHeight());
            articleImageButton.setMinimumWidth(thumbnail.getWidth());
        }
        final TextView extractTextView = holder.extract;
        extractTextView.setText(article.getExtract());
        final TextView numViewsTextView = holder.numViews;
        numViewsTextView.setText(String.valueOf(article.getViews()));
        final TextView rankValueTextView = holder.rankValue;
        rankValueTextView.setText(String.valueOf(article.getRank()));
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnAdapterInteractionListener onAdapterInteractionListener;

        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView displayTitle;
        TextView extract;
        TextView numViews;
        TextView rankValue;
        ImageButton articleImageButton;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(final View itemView, final OnAdapterInteractionListener listener) {
            super(itemView);
            onAdapterInteractionListener = listener;
            displayTitle = (TextView) itemView.findViewById(R.id.displaytitle_mostread);
            extract = (TextView) itemView.findViewById(R.id.extract_mostread);
            numViews = (TextView) itemView.findViewById(R.id.views_mostread);
            rankValue = (TextView) itemView.findViewById(R.id.rank_mostread);
            articleImageButton = (ImageButton) itemView.findViewById(R.id.imageButtonArticle);
            // Attach a click listener to the entire row view
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(final View view) {
            final int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                final Article article = articles.get(position);
                // We can access the data within the views
                if (article != null && article.getOriginalimage() != null) {
                    onAdapterInteractionListener.enlargePhotoOfSourceArticleClicked(article.getOriginalimage().getSource());
                } else {
                    Toast.makeText(view.getContext(), R.string.image_not_available, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
