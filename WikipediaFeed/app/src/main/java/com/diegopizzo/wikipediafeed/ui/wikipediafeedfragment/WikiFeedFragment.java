package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.diegopizzo.wikipediafeed.R;
import com.diegopizzo.wikipediafeed.business.network.model.Tfa;
import com.diegopizzo.wikipediafeed.business.network.model.Thumbnail;
import com.diegopizzo.wikipediafeed.business.network.model.WikiModel;
import com.diegopizzo.wikipediafeed.config.WikipediaFeedApplication;
import com.diegopizzo.wikipediafeed.config.mvp.AbstractMvpFragment;
import com.diegopizzo.wikipediafeed.ui.util.ImageUtils;
import com.jakewharton.rxbinding2.view.RxView;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;

/**
 * Created by diegopizzo on 26/10/2017.
 */

public class WikiFeedFragment extends AbstractMvpFragment<WikiFeedFragmentContract.Presenter> implements WikiFeedFragmentContract.View {

    public static final String TAG_WIKI_FEED_FRAGMENT = "WikiFeed";
    @BindView(R.id.most_read_recyclerview)
    RecyclerView mostReadRecyclerView;
    @BindView(R.id.tfa_displaytitle)
    TextView displayTitleTfa;
    @BindView(R.id.tfa_extract)
    TextView extractTfa;
    @BindView(R.id.imageViewTfa)
    ImageView imageViewTfa;
    @BindView(R.id.container_tfa)
    FrameLayout containerTfa;
    @BindView(R.id.expanded_image_tfa)
    ImageView expandedImageTfa;
    @BindView(R.id.tfa_card)
    CardView cardViewTfa;
    @Inject
    ImageUtils imageUtils;
    private WikiMostReadAdapter wikiMostReadAdapter;
    private OnFragmentInteractionListener onFragmentInteractionListener;


    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;


    OnAdapterInteractionListener listener = new OnAdapterInteractionListener() {
        @Override
        public void enlargePhotoOfSourceArticleClicked(final String path) {
            imageUtils.zoomImageFromThumb(imageViewTfa, path, expandedImageTfa,
                    getContext(), containerTfa, mShortAnimationDuration, progressBar);
        }
    };

    public static WikiFeedFragment newInstance(final Bundle bundle) {
        final WikiFeedFragment wikiFeedFragment = new WikiFeedFragment();
        if (bundle != null) {
            wikiFeedFragment.setArguments(bundle);
        }
        return wikiFeedFragment;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, final Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);
        presenter.wikiFeedNews();

        // Setup layout manager for items with orientation
        // Also supports `LinearLayoutManager.HORIZONTAL`
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        // Optionally customize the position you want to default scroll to
        layoutManager.scrollToPosition(0);
        // Attach layout manager to the RecyclerView
        mostReadRecyclerView.setLayoutManager(layoutManager);

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_longAnimTime);

        return view;
    }

    @Override
    public void setComponents(final WikiModel wikiModel) {
        if (wikiModel != null) {
            if (wikiModel.getTfa() != null) {
                final Tfa tfa = wikiModel.getTfa();
                displayTitleTfa.setText(tfa.getDisplaytitle());
                extractTfa.setText(tfa.getExtract());
                if (tfa.getThumbnail() != null) {
                    final Thumbnail thumbnail = tfa.getThumbnail();
                    imageViewTfa.setMinimumWidth(thumbnail.getWidth());
                    imageViewTfa.setMinimumHeight(thumbnail.getHeight());
                    imageViewTfa.setContentDescription(tfa.getDescription());
                    Glide.with(getContext()).load(tfa.getThumbnail().getSource()).into(imageViewTfa);

                    final Disposable disp = RxView.clicks(cardViewTfa)
                            .subscribe(aVoid -> {
                                imageUtils.zoomImageFromThumb(imageViewTfa, tfa.getOriginalimage().getSource(), expandedImageTfa,
                                        getContext(), containerTfa, mShortAnimationDuration, progressBar);
                            });
                }
            }
            if (wikiModel.getMostread() != null && wikiModel.getMostread().getArticles() != null) {
                wikiMostReadAdapter = new WikiMostReadAdapter(wikiModel.getMostread().getArticles(), getContext(), listener);
                mostReadRecyclerView.setAdapter(wikiMostReadAdapter);
            }
        }
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            onFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentInteractionListener = null;
    }

    @Override
    protected void inject() {
        DaggerWikiFeedFragmentComponent.builder()
                .applicationComponent(((WikipediaFeedApplication) getActivity().getApplication()).getApplicationComponent())
                .wikiFeedFragmentModule(new WikiFeedFragmentModule(this)).build().inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.wiki_feed_layout;
    }

    public interface OnFragmentInteractionListener {
    }
}
