package com.diegopizzo.wikipediafeed.config.dagger;

import android.content.Context;

import com.diegopizzo.wikipediafeed.business.interactor.WikiInteractor;
import com.diegopizzo.wikipediafeed.config.WikipediaFeedApplication;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by diegopizzo on 25/09/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class, InteractorModule.class, NetworkModule.class})
public interface ApplicationComponent {

    WikiInteractor providesWikiServiceInteractor();

    class Injector {
        public static ApplicationComponent getComponent(final Context c) {
            return ((WikipediaFeedApplication) c.getApplicationContext()).getApplicationComponent();
        }
    }
}
