package com.diegopizzo.wikipediafeed.business.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by diegopizzo on 25/10/2017.
 */

public class Mostread {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("articles")
    @Expose
    private List<Article> articles;

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public Mostread withDate(final String date) {
        this.date = date;
        return this;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(final List<Article> articles) {
        this.articles = articles;
    }

    public Mostread withArticles(final List<Article> articles) {
        this.articles = articles;
        return this;
    }

}
