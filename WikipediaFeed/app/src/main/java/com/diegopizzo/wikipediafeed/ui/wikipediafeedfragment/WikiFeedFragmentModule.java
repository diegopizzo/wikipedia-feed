package com.diegopizzo.wikipediafeed.ui.wikipediafeedfragment;

import com.diegopizzo.wikipediafeed.business.interactor.WikiInteractor;
import com.diegopizzo.wikipediafeed.config.dagger.FragmentScope;
import com.diegopizzo.wikipediafeed.ui.util.ImageUtils;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegopizzo on 26/10/2017.
 */

@Module
public class WikiFeedFragmentModule {

    private final WikiFeedFragmentContract.View view;

    public WikiFeedFragmentModule(final WikiFeedFragmentContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    public WikiFeedFragmentContract.Presenter providePresenter(final WikiInteractor wikiInteractor) {
        return new WikiFeedFragmentPresenter(view, wikiInteractor);
    }

    @FragmentScope
    @Provides
    public ImageUtils provideImageUtils() {
        return new ImageUtils();
    }
}
