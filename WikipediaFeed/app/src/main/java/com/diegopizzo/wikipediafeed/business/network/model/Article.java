package com.diegopizzo.wikipediafeed.business.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by diegopizzo on 25/10/2017.
 */

public class Article {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("normalizedtitle")
    @Expose
    private String normalizedtitle;
    @SerializedName("views")
    @Expose
    private int views;
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;
    @SerializedName("originalimage")
    @Expose
    private Originalimage originalimage;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("extract")
    @Expose
    private String extract;

    public Originalimage getOriginalimage() {
        return originalimage;
    }

    public void setOriginalimage(final Originalimage originalimage) {
        this.originalimage = originalimage;
    }

    public Article withOriginalimage(final Originalimage originalimage) {
        this.originalimage = originalimage;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Article withTitle(final String title) {
        this.title = title;
        return this;
    }

    public String getNormalizedtitle() {
        return normalizedtitle;
    }

    public void setNormalizedtitle(final String normalizedtitle) {
        this.normalizedtitle = normalizedtitle;
    }

    public Article withNormalizedtitle(final String normalizedtitle) {
        this.normalizedtitle = normalizedtitle;
        return this;
    }

    public int getViews() {
        return views;
    }

    public void setViews(final int views) {
        this.views = views;
    }

    public Article withViews(final int views) {
        this.views = views;
        return this;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(final int rank) {
        this.rank = rank;
    }

    public Article withRank(final int rank) {
        this.rank = rank;
        return this;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(final Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Article withThumbnail(final Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Article withDescription(final String description) {
        this.description = description;
        return this;
    }

    public String getExtract() {
        return extract;
    }

    public void setExtract(final String extract) {
        this.extract = extract;
    }

    public Article withExtract(final String extract) {
        this.extract = extract;
        return this;
    }

}
