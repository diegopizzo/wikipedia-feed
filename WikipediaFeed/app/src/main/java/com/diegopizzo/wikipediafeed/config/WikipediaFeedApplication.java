package com.diegopizzo.wikipediafeed.config;

import android.app.Application;

import com.diegopizzo.wikipediafeed.config.dagger.ApplicationComponent;
import com.diegopizzo.wikipediafeed.config.dagger.ApplicationModule;
import com.diegopizzo.wikipediafeed.config.dagger.DaggerApplicationComponent;
import com.jakewharton.threetenabp.AndroidThreeTen;

/**
 * Created by diegopizzo on 29/10/2017.
 */

public class WikipediaFeedApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
