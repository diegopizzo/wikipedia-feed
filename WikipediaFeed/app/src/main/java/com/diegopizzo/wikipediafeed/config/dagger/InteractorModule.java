package com.diegopizzo.wikipediafeed.config.dagger;

import com.diegopizzo.wikipediafeed.business.interactor.WikiInteractor;
import com.diegopizzo.wikipediafeed.business.network.WikiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by diegopizzo on 27/09/2017.
 */

@Module
public class InteractorModule {

    @Provides
    @Singleton
    WikiInteractor providesWikiServiceInteractor(final WikiService wikiService) {
        return new WikiInteractor(wikiService);
    }
}
